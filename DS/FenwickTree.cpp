#include<bits/stdc++.h>
using namespace std;

// Binary Indexed Tree (BIT) aka Fenwick Tree
// Создание/использование:
// Fen fen(a); // a - vector<int>
// fen.sum(i);
// fen.sum(i, j);
// fen.inc(i, x);

struct Fen {
    vector<int> f; 
    int n;

    Fen(int nn){
        this->n = nn;
        f.assign(n, 0);
    }

    Fen(vector<int> const &a) : Fen((int)a.size()) {
        for (size_t i = 0; i < a.size(); i++)
            inc(a[i], 1);
    }

    // sum(0..r) - сумма на отрезке [0, r]
    int sum(int r) {
        int ret = 0;
        for (; r >= 0; r = (r & (r + 1)) - 1)
            ret += f[r];
        return ret;
    }

    // sum(l..r) - сумма на отрезке [l, r]
    int sum(int l, int r) {
        return sum(r) - sum(l - 1);
    }

    // a[i] += x  через обновление f 
    void inc(int i, int x) {
        for (; i < n; i = i | (i + 1))
            f[i] += x;
    }
};


int32_t main() {

    return 0;
}
