#include<bits/stdc++.h>

using namespace std;
using ull = unsigned long long;
using ll  = long long;

const int maxn = 2e5 + 1;
int p[maxn];
int r[maxn];

void build_dsu(int n){
    for (int i = 1; i <= n; i++){
        p[i] = i;
    }
}
int get(int a){
    return p[a] = (a == p[a] ? a : get(p[a]));
}

void unite(int a, int b){
    a = get(a);
    b = get(b);
    if(a == b)
    return;
    if(r[a] == r[b]){
        r[a]++;
    }
    if(r[a] > r[b]){
        p[b] = a;
    }
    else{
        p[a] = b;
    }
}


int32_t main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    return 0;
}
