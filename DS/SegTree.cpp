#include<bits/stdc++.h>

using namespace std;
// size + (size - 1)
// size - размер исходного массива "добитый" до степени двойки
// (size - 1) - кол-во вершина ДО, не являющихся листами

using ll = long long;

/* 1. a[i] = v;
 * 2. sum(l, r) <==> sum([l, r-1]); */
struct segtree{
    vector<ll> tree;
    int size;

    void init(int n){
        size = 1;
        while(size < n) size <<= 1;
        tree.assign(2*size - 1, 0);
    }

    void build(vector<int>& a){
        init(a.size());
        build(a, 0, 0, size);
    }
    
    void set(int i, int v){ // a[i] = v;
        set(i, v, 0, 0, size);
    }

    ll sum(int l, int r){
        return sum(l, r, 0, 0, size); 
    }

    void build(vector<int>& a, int x, int lx, int rx){
        if(lx + 1 == rx){
            if(lx < (int)a.size())
                tree[x] = a[lx];
            return;
        }
        int m = lx + (rx - lx) / 2;
        build(a, 2*x + 1, lx, m);
        build(a, 2*x + 2, m, rx);
        tree[x] = tree[2*x + 1] + tree[2*x + 2];
    }

    void set(int i, int v, int x, int lx, int rx){
        if(lx + 1 == rx){
            tree[x] = v;
            return;
        }

        int m = lx + (rx - lx) / 2;
        if(i < m)
            set(i, v, 2*x + 1, lx, m);
        else
            set(i, v, 2*x + 2, m, rx);

        tree[x] = tree[2*x + 1] + tree[2*x + 2];
    }

    ll sum(int l, int r, int x, int lx, int rx){
        if(lx >= r || rx <= l)
            return 0;
        if(lx >= l && rx <= r)
            return tree[x];

        int m = lx + (rx - lx) / 2;
        ll sum1 = sum(l, r, 2*x + 1, lx, m);
        ll sum2 = sum(l, r, 2*x + 2, m, rx);

        return sum1 + sum2;
    }
};

int32_t main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int n, m;
    cin >> n >> m;

    vector<int> a(n);
    for(auto &x : a) cin >> x;

    segtree st;
    st.build(a);

    // ...

    return 0;
}
