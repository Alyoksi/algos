#include<bits/stdc++.h>
using namespace std;

#define vi vector<int> 
const int INF = 2e9;

// set(l, r, v) - присвоение на отрезке
// _min(l, r)   - минимум на отрезке
struct SegT{
    vi tr, lazy;
    int size = 0;

    void init(int n, int v){
        size = 1;
        while(size < n) size <<= 1;
        tr.assign(2*size-1, v);
        lazy.assign(2*size-1, -1);
    }

    void push(int x){
        if(lazy[x] == -1) return;
        lazy[2*x+1] = tr[2*x+1] = lazy[x];
        lazy[2*x+2] = tr[2*x+2] = lazy[x];
        lazy[x] = -1;
    }

    void set(int l, int r, int v){
        set(l, r, v, 0, 0, size);
    }

    void set(int l, int r, int v, int x, int lx, int rx){
        if(lx >= r || rx <= l) return;
        if(l <= lx && rx <= r){
            lazy[x] = tr[x] = v;
            return;
        }
        push(x);

        int m = (lx + rx) >> 1;
        set(l, r, v, 2*x+1, lx, m);
        set(l, r, v, 2*x+2, m, rx);

        tr[x] = min(tr[2*x+1], tr[2*x+2]);
    }

    int _min(int l, int r){
        return _min(l, r, 0, 0, size);
    }

    int _min(int l, int r, int x, int lx, int rx){
        if(lx >= r || rx <= l) return INF;
        if(l <= lx && rx <= r){
            return tr[x];
        }
        push(x);

        int m = (lx + rx) >> 1;
        return min(_min(l, r, 2*x+1, lx, m), _min(l, r, 2*x+2, m, rx));
    }
};


int32_t main() {
    ios::sync_with_stdio(0);
    cin.tie(0);

    return 0;
}

