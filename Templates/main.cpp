#include<bits/stdc++.h>

using namespace std;
using ull = unsigned long long;
using ll  = long long;
#define pb emplace_back
#define fi first 
#define se second
#define vi vector<int> 
#define vl vector<ll> 
#define vvi vector<vi> 
#define vvl vector<vl> 
#define pii pair<int,int>
#define pll pair<ll, ll>
#define vii vector<pii> 
#define vll vector<pll> 
#define vvii vector<vii>
#define vvll vector<vll>
#define all(x) begin(x), end(x)
const int INF = 2e9;
const ll INFL = 9e18;

void solve(){

}

int32_t main() {
    ios::sync_with_stdio(0);
    cin.tie(0);

    //freopen(".in", "r", stdin);
    //freopen(".out", "w", stdout);
    
    int t = 1;
    // cin >> t;

    while(t--) solve();

    return 0;
}
