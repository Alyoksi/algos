#include<bits/stdc++.h>
using namespace std;

#define fi first 
#define se second
#define vi vector<int> 
#define pii pair<int, int>
#define all(x) (x).begin(), (x).end()
#define sz(x) int((x).size())

void count_sort(vi& p, vi& c, int n){
    vi cnt(n+1, 0), new_p(n);
    for(auto x : c) cnt[x+1]++;
    // pos[i] = cnt[i-1] + pos[i-1]; 
    for(int i = 1; i <= n; i++) cnt[i] += cnt[i-1];

    for(auto x : p) new_p[cnt[c[x]]++] = x; 
    p.swap(new_p);
}

int32_t main() {
    string s; cin >> s;

    s += '$';
    int n = sz(s);

    vi p(n), c(n, 0);
    {
        // k = 0
        vector<pair<char, int>> a(n);
        for(int i = 0; i < n; ++i) a[i] = {s[i], i};
        sort(all(a));
        for(int i = 0; i < n; i++) p[i] = a[i].se;
        for(int i = 1; i < n; i++) c[p[i]] = c[p[i-1]] + (a[i].fi != a[i-1].fi);
    }

    int k = 0;
    while((1<<k) < n){
        // k -> k+1
        
        // Сортируем по второй части, т.е. s[i+2**k : i+2**(k+1)-1]
        for(int i = 0; i < n; i++)
            p[i] = (p[i] - (1<<k) + n) % n;
        // Сортируем по первой части, т.е. s[i : i+2**k-1]
        count_sort(p, c, n);

        // Переопределяем группы
        vi new_c(n, 0);
        for(int i = 1; i < n; i++){
            pii pr = {c[p[i-1]], c[(p[i-1] + (1<<k))%n]};
            pii cur = {c[p[i]], c[(p[i] + (1<<k))%n]};
            new_c[p[i]] = new_c[p[i-1]] + (cur != pr);
        }
        c.swap(new_c);
        k++;
    }

    // Алгоритм Касаи: ищем LCP соседних суффиксов в отсорт. порядке
    k = 0;
    vi lcp(n, 0);
    for(int i = 0; i+1 < n; i++){ // i+1 < n !!! Потому что у "$" нет суффикса
        int pi = c[i]; // позиция суффикса в отсорт. порядке
        int j = p[pi - 1]; // какой суффикс до нас

        while(s[i+k] == s[j+k]) k++;
        lcp[pi] = k; // именно pi, потому что строим относительно суфф. масс.
        k = max(0, k-1);
    } 

    for(int i = 0 ; i < n; i++)
        cout << "(" << lcp[i] << ", " << p[i] << ") : " << s.substr(p[i], n-p[i]) << '\n';

    return 0;
}

