#include<bits/stdc++.h>

using namespace std;

vector<int> build(string& s){
    int n = s.size();
    vector<int> a(n, 0);

    int l = 0, r = 0;
    for(int i = 1; i < n; i++){
        if(r >= i)
            a[i] = min(r-i+1, a[i-l]);
        while(i+a[i]<n && s[i+a[i]] == s[a[i]])
            a[i]++;
        if(r < i+a[i]-1)
            l = i, r = i+a[i]-1;
    }
    return a;
}

int32_t main() {
    ios::sync_with_stdio(0);
    cin.tie(0);

    return 0;
}
