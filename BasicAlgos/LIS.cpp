#include<bits/stdc++.h>
using namespace std;

#define vi vector<int>
#define pb push_back

// LIS - Longest Increasing Subsequence
// LNDS - Longest Non-Decreasing Subsequence


// lower_bound  - LIS
// upper_bound  - LNDS

vi d;
void solve(){
    int n; cin >> n;
    for (int i = 0; i < n; i++) {
        int x; cin >> x;

        vi::iterator it = upper_bound(d.begin(), d.end(), x);

        if(it == d.end())
            d.pb(x);
        else
            *it = x;
    }
    cout << "LNDS = " << d.size() << '\n';
}

int32_t main() {
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t = 1;
    // cin >> t;

    while(t--) solve();

    return 0;
}
