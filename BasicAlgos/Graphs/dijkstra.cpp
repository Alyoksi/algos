#include<bits/stdc++.h>
using namespace std;
const ll INFL = 9e18;

int main(){
    
    /* Считываем граф */
    vl dist(n, INFL);
    priority_queue<pll> q;
    dist[s] = 0;
    q.push({0, s});

    while(!q.empty()){
        ll len = -q.top().fi;
        int v = q.top().se;
        q.pop();
        if(len > dist[v]) continue;

        for(auto [to, w] : g[v]){
            if(dist[to] > dist[v] + w){
                dist[to] = dist[v] + w;
                q.push({-dist[to], to});
            }
        }
    }


    return 0;
}

