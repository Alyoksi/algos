#include<bits/stdc++.h>
using namespace std;
const int INF = 2e9;
int32_t main() {

    int n, s; cin >> n >> s;
    vector<vector<pii>> g;

    vector<int> dist(n, INF);
    dist[s] = 0;
    deque<int> q;
    q.push_front(s);
    while (!q.empty()) {
        int v = q.front();
        q.pop_front();
        for (auto [to, w] : g[v]) {
            if (dist[v] + w < dist[to]) {
                dist[to] = dist[v] + w;
                if (w == 1)
                    q.push_back(to);
                else
                    q.push_front(to);
            }
        }
    }


    return 0;
}
